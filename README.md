# i3config

My i3 wm configs

Included here is also polybar and compton conf.  
You'd also have to link those to their approps places if you wanted to use them

polybar -> ~/.config/polybar/
compton -> ~/.config/compton.conf

This has a scratch terminal bound to meta-u
navigation and resizing is vimmy with hjkl

Take a screenshot with scrot using meta-shift-s (then click/drag to select area)

